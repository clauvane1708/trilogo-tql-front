import React from "react";
import LocaleProvider from 'antd/lib/locale-provider';
import ptBR from 'antd/lib/locale-provider/pt_BR';
import { Switch, Route } from 'react-router'

import Home from './home/home'
import About from './about/about'
import Tql from './tql/container/tql'

const Routes = () => (
        <Switch>
            <LocaleProvider locale={ptBR}>
                <div>
                    <Route exact path='/' component={Home} />
                    <Route path='/home' component={Home} />
                    <Route path='/about' component={About} />
                    <Route path='/tql' component={Tql} />
                </div>
            </LocaleProvider>
        </Switch>
);

export default Routes;