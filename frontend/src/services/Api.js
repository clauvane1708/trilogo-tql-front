import apisauce from 'apisauce';

const create = (baseURL = 'http://localhost:3001') => {
  const api = apisauce.create({
    baseURL,
    timeout: 15000
  });

  const Tql = {
    init: () => api.get(`/tql`),
    search: (tql) => {
      api.setBaseURL('https://api.trilogo.com.br')
      return api.get(`/tickets?tql=${tql}`)
    }
  };

  return {
    Tql
  }
}

export default {
  create
}
