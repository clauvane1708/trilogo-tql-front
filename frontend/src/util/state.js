/* 
    States for CRUD
*/
export const SEARCHING = "SEARCHING"
export const INSERTING = "INSERTING"
export const EDITING   = "EDITING"

/* 
    States for PLAY
*/
export const CHOOSING   = "CHOOSING"
export const PLAYING    = "PLAYING"
export const RESULTING  = "RESULTING"