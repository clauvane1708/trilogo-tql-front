import { call, put, select } from 'redux-saga/effects'
import TqlActions from './redux';
import { get } from "lodash";

const msgError  = {tipo: 'error', descricao: "Erro inesperado, por favor, entre em contato com o administrador do sistema."}

export function * fetch (api, action)  {
  try {    
      const response = yield call(api.Tql.init)
      if (response.ok) {
        const initialState = get(response, ['data'], {})
        yield put(TqlActions.success(initialState))
      } else {
        yield put(TqlActions.failure(msgError))
     }
  } catch (ex) {
    yield put(TqlActions.failure(msgError))
  }
}

export function * search (api, action)  {
  try {    
      const tql = yield select(state => state.tql.tql)
      const response = yield call(api.Tql.search, tql)
      if (response.ok) {
        const result = get(response, ['data'], {})
        yield put(TqlActions.success(result))
      } else {
        yield put(TqlActions.failure(msgError))
     }
  } catch (ex) {
    yield put(TqlActions.failure(msgError))
  }
}
