import React, { Component } from 'react'
import { connect } from 'react-redux'
import TqlActions from '../redux'
import { Spin } from 'antd'
import { isEmpty, get } from 'lodash'
import { openNotification } from '../../util/notification'
import TqlSearch from '../components/tqlSearch'
import TqlTable from '../components/tqlTable'

class Tql extends Component {
    
    componentDidMount() {
        this.props.init()
    }

    componentWillReceiveProps(nextProps){
    	const message = get(nextProps, ['message'], "")
    	if (!isEmpty(message)) {
            openNotification(message)
            this.props.cleanMessage()
		   }
    }

    render() {
        const { fetching } = this.props
        return (
            <Spin spinning={ fetching }>
                <TqlSearch />
                <TqlTable />
            </Spin>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        ...state.tql.data,
        fetching: state.tql.fetching
    }
}

const mapDispatchToProps = (dispatch) => ({
    init: ()  => dispatch(TqlActions.init()),
    cleanMessage: ()  => dispatch(TqlActions.cleanMessage())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tql)