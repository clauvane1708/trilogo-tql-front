import React, { Component } from 'react'
import { connect } from 'react-redux'
import TqlActions from '../redux'
import { Card, Form, Button, Select, Input, Row, Col } from 'antd'
import { generateOptions } from '../../util/helper'

class TqlSearch extends Component {
    
    search = () => {
        const { search } = this.props
        search()
    }

    clean = () => {
        const { form: { resetFields }, setTql } = this.props
        resetFields();
        setTql(null);
    }

    add = (e) => {
        e.preventDefault();
        const { form: { validateFields, setFieldsValue, getFieldValue }, tql, setTql } = this.props
        validateFields((err, values) => {
          if (!err) {
            let field = this.getLabel("field");
            let operator = this.getLabel("operator");
            let keyword = this.getLabel("keyword");
            let value = getFieldValue("value");
            let query = `${tql ? tql : ''} ${field} ${operator} ${value} ${keyword ? keyword : ''}`
            setTql(query)
            setFieldsValue({
                tql: query,
                field: null,
                operator: null,
                value: null,
                keyword: null
            })
          }
        });
    }

    getLabel = (field) => {
        const { form: { getFieldInstance }} = this.props
        const fieldInstance = getFieldInstance(field);
        const value = fieldInstance.props.value;
        return value ? fieldInstance.rcSelect.getLabelBySingleValue(value) : ''
    }

    render() {
        let { tql, fields, keywords, operators, form: { getFieldDecorator }} = this.props
        return (
                <Card title={ "Trílogo" } 
                      style={{ marginBottom: '10px'}}>
                      <Form onSubmit={this.add} layout={"vertical"}>
                        <Row gutter={16}>
                            <Col span={"4"}>
                                <Form.Item label={"Field"}>
                                {getFieldDecorator('field', {
                                    rules: [{ required: true, message: 'Please input your field!' }],
                                    initialValue: null
                                })(
                                    <Select>
                                        <Select.Option value={ null }>Selecione</Select.Option>
                                        { generateOptions(fields) }
                                    </Select>
                                )}
                                </Form.Item>
                            </Col>
                            <Col span={"4"}>
                                <Form.Item label={"Operator"}>
                                {getFieldDecorator('operator', {
                                    rules: [{ required: true, message: 'Please input your operator!' }],
                                    initialValue: null
                                })(
                                    <Select>
                                        <Select.Option value={ null }>Selecione</Select.Option>
                                        { generateOptions(operators) }
                                    </Select>
                                )}
                                </Form.Item>
                            </Col>
                            <Col span={"12"}>
                                <Form.Item label={"Value(s)"}>
                                {getFieldDecorator('value', {
                                    rules: [
                                        { required: true, message: 'Please input your value(s)!' },
                                        { max: 50, message: 'Maxlength is 50 characters.'},
                                        { whitespace: true }
                                    ],
                                    initialValue: null
                                })(
                                    <Input />
                                )}
                                </Form.Item>
                            </Col>
                            <Col span={"4"}>
                                <Form.Item label={"Keyword"}>
                                {getFieldDecorator('keyword', {
                                    rules: [],
                                    initialValue: null
                                })(
                                    <Select>
                                        <Select.Option value={ null }>Selecione</Select.Option>
                                        { generateOptions(keywords) }
                                    </Select>
                                )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={ 16 } >
                            <Col span={"6"}>
                                <Button type="default" htmlType="button" onClick={this.clean}>
                                    Clean
                                </Button>
                                &nbsp;
                                <Button type="primary" htmlType="submit">
                                    ADD
                                </Button>
                            </Col>
                        </Row>
                        <Row gutter={ 16 } style = {{marginTop: '10px'}}>
                            <Col span={"24"}>
                                <Form.Item label={"Query"}>
                                {getFieldDecorator('tql', {
                                    rules: [],
                                    initialValue: null
                                })(
                                    <Input disabled/>
                                )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Button type="primary" 
                                    htmlType="button" 
                                    onClick={this.search}
                                    disabled={tql ? false : true}> 
                                Search
                            </Button>
                        </Row>
                       </Form>
                </Card>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        search: () => dispatch(TqlActions.search()),
        setTql: (tql) => dispatch(TqlActions.setTql(tql))
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.tql.data,
        fetching: state.tql.fetching,
        tql: state.tql.tql
    }
}

const WrappedSearch = Form.create()(TqlSearch);

export default connect(mapStateToProps, mapDispatchToProps)(WrappedSearch)