import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, Table } from 'antd'

const columns = [{
    title: 'Project',
    dataIndex: 'project',
    key: 'project',
  },{
    title: 'Type',
    dataIndex: 'type',
    key: 'type',
  },{
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  }, {
    title: 'Assignee',
    dataIndex: 'assignee',
    key: 'assignee',
  },{
    title: 'Term',
    dataIndex: 'term',
    key: 'term',
  }];

class TqlTable extends Component {
    
    render() {
        let { tickets } = this.props
        return (
                <Card title={ "Tickets" } >
                      <Table rowKey={(t) => t.id} dataSource={tickets} columns={columns}/>
                </Card>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.tql.data,
        fetching: state.tql.fetching,
        tickets: state.tql.tickets
    }
}

export default connect(mapStateToProps, null)(TqlTable)