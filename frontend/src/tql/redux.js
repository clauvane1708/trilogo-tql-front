import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { get } from "lodash"

const { Types, Creators } = createActions({
   init: null,
   success: ['dados'],
   search: null,
   failure: ['msgError'],
   cleanMessage: null,
   setTql: ['tql']
});

export const TqlTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
    data:  {
      fields: [],
      keywords: [],
      operators: [],
      tickets: []
    },
    fetching: false,
    tql: null
});

export const request = (state) => state.merge({ fetching: true })
export const success = (state, { dados }) =>  {

  let data = {
    fields:     get(dados, ['fields'],    get(state.data, ['fields'], [])),
    keywords:   get(dados, ['keywords'],  get(state.data, ['keywords'], [])),
    operators:  get(dados, ['operators'], get(state.data, ['operators'], [])),
    message:    get(dados, ['message'],   get(state.data, ['message'], [])),
    tickets:    get(dados, ['tickets'],   get(state.data, ['tickets'], [])),
  }

   state = state.merge({fetching: false, data})
   return state
}

export const failure = (state, {msgError}) => {
  return state.merge({fetching: false, data: {...state.data, message: msgError}})
}

export const cleanMessage = (state, action) => state.merge({data: {...state.data, message: ""}})

export const setTql = (state, { tql }) => state.merge({tql})

export const reducer = createReducer(INITIAL_STATE, {
  [Types.INIT]          : request,
  [Types.SUCCESS]       : success,
  [Types.SEARCH]        : request,
  [Types.FAILURE]       : failure,
  [Types.CLEAN_MESSAGE] : cleanMessage,
  [Types.SET_TQL]       : setTql
})
