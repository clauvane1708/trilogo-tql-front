import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    tql: require('../tql/redux').reducer,
});

export default rootReducer;
