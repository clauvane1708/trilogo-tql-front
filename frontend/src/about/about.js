import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div>
                <h4>Trilogo</h4>
                <hr/>
                <h2>Nascemos modernos. Continuaremos assim.</h2>
                <p>
                {
                `Dinâmica, performance e inovação conduzem nosso cotidiano. 
                 Baseados nesses pilares, entregamos a nossos clientes softwares 
                 com tecnologia em nuvem e serviços de alto valor agregado.`
                }
                </p>
            </div>
        )
    }
}